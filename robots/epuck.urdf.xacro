<?xml version = "1.0" ?>
<robot name="epuck" 
  xmlns:xacro="http://www.ros.org/wiki/xacro">

  <xacro:macro name="epuck_laser" params="name angle">
    <xacro:property name="radius" value="0.036" />

    <link name="${name}_laser_link" />

    <joint name= "base_link_to_${name}_laser" type="fixed">
      <origin xyz="${radius * cos(radians(angle))} ${radius * sin(radians(angle))} 0.034" rpy="0 0 ${radians(angle)}"/>
      <parent link="base_link"/>
      <child link="${name}_laser_link"/>
    </joint>

    <gazebo reference="${name}_laser_link">
      <sensor type="ray" name="${name}_laser">
        <visualize>true</visualize>
        <update_rate>10</update_rate>
        <ray>
          <scan>
            <horizontal>
              <samples>1</samples>
              <min_angle>0</min_angle>
              <max_angle>0</max_angle>
            </horizontal>
          </scan>
          <range>
            <min>0.001</min>
            <max>0.06</max>
            <resolution>0.001</resolution>
          </range>
        </ray>
        <plugin name="gazebo_ros_front_laser_controller" filename="libgazebo_ros_laser.so">
          <!-- <robotNamespace></robotNamespace> -->
          <topicName>/epuck/lasers/${name}</topicName>
          <frameName>${name}_laser_link</frameName>
          <alwaysOn>true</alwaysOn>
          <gaussianNoise>0.00</gaussianNoise>
          <updateRate>10.0</updateRate>
        </plugin>
      </sensor>
    </gazebo>
  </xacro:macro>

  <!-- base_footprint is a fictitious link(frame) that is on the ground right below base_link origin, navigation stack depends on this frame -->
  <link name="base_footprint" />

  <link name="base_link">
    <inertial>
      <origin xyz="9.0274E-05 -0.00041659 0.019845" rpy="0 0 0" />
      <!-- With a realistic mass, the robot slides by itself. Just a hack before a proper solution is found  -->
      <!-- <mass value="0.063515" /> -->
      <mass value="0.0001" />
      <inertia ixx="3.2227E-05" ixy="-2.691E-07" ixz="4.7122E-08" iyy="2.4227E-05" iyz="-1.2861E-06" izz="3.3868E-05" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/main_body.STL" />
      </geometry>
      <material name="">
        <color rgba="0.75294 0.75294 0.75294 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/main_body.STL" />
      </geometry>
    </collision>
  </link>

  <!-- <link name="main_body">
  </link> -->

  <link name="left_wheel">
    <inertial>
      <origin xyz="-0.00048709 -3.5245E-18 2.4286E-17" rpy="0 0 0" />
      <mass value="0.0040981" />
      <inertia ixx="8.267E-07" ixy="-9.8761E-23" ixz="-8.6429E-13" iyy="4.304E-07" iyz="-2.3376E-22" izz="4.304E-07" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/left_wheel.STL" />
      </geometry>
      <material name="">
        <color rgba="0.79216 0.81961 0.93333 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/left_wheel.STL" />
      </geometry>
    </collision>
  </link>

  <link name="right_wheel">
    <inertial>
      <origin xyz="0.00065056 4.0415E-18 7.2858E-17" rpy="0 0 0" />
      <mass value="0.0038059" />
      <inertia ixx="8.2274E-07" ixy="-1.0753E-22" ixz="8.6429E-13" iyy="4.2585E-07" iyz="2.3307E-22" izz="4.2585E-07" />
    </inertial>
    <visual>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/right_wheel.STL" />
      </geometry>
      <material name="">
        <color rgba="0.79216 0.81961 0.93333 1" />
      </material>
    </visual>
    <collision>
      <origin xyz="0 0 0" rpy="0 0 0" />
      <geometry>
        <mesh filename="package://epuck_umrob/meshes/right_wheel.STL" />
      </geometry>
    </collision>
  </link>

  <link name="camera_link"/>

  <joint name="base_footprint_joint" type="fixed">
    <origin xyz="0 0 0" rpy="0 0 0" />
    <parent link="base_footprint"/>
    <child link="base_link" />
  </joint>

  <!-- <joint name= "base_link_to_base_link" type = "fixed">
    <parent link = "base_link"/>
    <child link = "base_link"/>
  </joint> -->

  <joint name="left_joint" type="continuous">
    <origin xyz="0.0278 0 0.02" rpy="0 0 0" />
    <parent link="base_link" />
    <child link="left_wheel" />
    <axis xyz="1 0 0" />
    <limit effort="0.001" velocity="10" />
  </joint>

  <joint name="right_joint" type="continuous">
    <origin xyz="-0.0278 0 0.02" rpy="0 0 0" />
    <parent link="base_link" />
    <child link="right_wheel" />
    <axis xyz="1 0 0" />
    <limit effort="0.001" velocity="10" />
  </joint>

  <joint name= "base_link_to_camera" type="fixed">
    <origin xyz="0 0.0307 0.03" rpy="1.57 0 0"/>
    <parent link="base_link"/>
    <child link="camera_link"/>
  </joint>

  <transmission name="right_motor_t">
    <type>transmission_interface/SimpleTransmission</type>
    <actuator name="right_motor_m">
      <mechanicalReduction>50</mechanicalReduction>
    </actuator>
    <joint name="right_joint">
      <hardwareInterface>hardware_interface/VelocityJointInterface</hardwareInterface>
    </joint>
  </transmission>

  <transmission name="left_motor_t">
    <type>transmission_interface/SimpleTransmission</type>
    <actuator name="left_motor_m">
      <mechanicalReduction>50</mechanicalReduction>
    </actuator>
    <joint name="left_joint">
      <hardwareInterface>hardware_interface/VelocityJointInterface</hardwareInterface>
    </joint>
  </transmission>

  <gazebo>
    <plugin name="gazebo_ros_control" filename="libgazebo_ros_control.so">
      <robotNamespace>/epuck</robotNamespace>
      <alwaysOn>true</alwaysOn>
      <updateRate>1000.0</updateRate>
    </plugin>
  </gazebo>

  <gazebo reference="base_link">
    <material>Gazebo/Grey</material>
  </gazebo>

  <gazebo reference="camera_link">
    <sensor type="camera" name="camera1">
      <update_rate>10.0</update_rate>
      <camera name="camera">
        <horizontal_fov>1.3962634</horizontal_fov>
        <image>
          <width>640</width>
          <height>480</height>
          <format>R8G8B8</format>
        </image>
        <clip>
          <near>0.02</near>
          <far>300</far>
        </clip>
      </camera>
      <plugin name="camera_controller" filename="libgazebo_ros_camera.so">
        <robotNamespace>/epuck</robotNamespace>
        <alwaysOn>true</alwaysOn>
        <updateRate>0.0</updateRate>
        <cameraName>camera</cameraName>
        <imageTopicName>image_raw</imageTopicName>
        <cameraInfoTopicName>camera_info</cameraInfoTopicName>
        <frameName>camera_link</frameName>
        <hackBaseline>0.07</hackBaseline>
        <distortionK1>0.0</distortionK1>
        <distortionK2>0.0</distortionK2>
        <distortionK3>0.0</distortionK3>
        <distortionT1>0.0</distortionT1>
        <distortionT2>0.0</distortionT2>
      </plugin>
    </sensor>
  </gazebo>

  <xacro:epuck_laser name="ps0" angle="75" />
  <xacro:epuck_laser name="ps1" angle="45" />
  <xacro:epuck_laser name="ps2" angle="0" />
  <xacro:epuck_laser name="ps3" angle="300" />
  <xacro:epuck_laser name="ps4" angle="240" />
  <xacro:epuck_laser name="ps5" angle="180" />
  <xacro:epuck_laser name="ps6" angle="135" />
  <xacro:epuck_laser name="ps7" angle="105" />

  <gazebo reference="right_wheel">
    <selfCollide>true</selfCollide>
    <mu1>10</mu1>
    <mu2>10</mu2>
    <material>Gazebo/White</material>
  </gazebo>

  <gazebo reference="left_wheel">
    <selfCollide>true</selfCollide>
    <mu1>10</mu1>
    <mu2>10</mu2>
    <material>Gazebo/White</material>
  </gazebo>

</robot>

